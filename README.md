# Draw IO Diagrams  

## Diagrams  

These are diagrams I have made in Draw.io for my MSC in Digital and Tech at Northumbria University. I have had trouble finding diagrams I could use in Draw IO so I hope these might be helpful for somebody.  

### List of diagrams:  
* Porters Five Force Model (Slightly amended headings for my own use case)  
* Stakeholder Analysis (Adapted from Strategic Management of Technological Innovation by Melissa Schilling)  
* Gibb's Reflective Framework  
* Chesborough Open Innovation Funnel (Funnily enough adapted from Henry Chesbrough's book Open services innovation : rethinking your business to grow and compete in a new era)
* Basic Balanced Scorecard (Not a fan of this model but it might be useful to someone)  
* Kolb's Experiential Reflective Cycle - my preferred reflective model as it has multitudes of interpretations. A basic flow chart.  
* SWOT Analysis - Strengths, Weaknesses, Objectives and Threats to analyse areas of improvement.
